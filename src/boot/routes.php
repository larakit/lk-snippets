<?php
//################################################################################
//  роуты
//################################################################################
define('ROUTE_ADMIN_SNIPPET', 'admin.snippet');
$r          = \Larakit\CRUD\Route::crud(ROUTE_ADMIN_SNIPPET, 'admincp/snippets', 'fa fa-folder-open', null, '\Larakit\Http\Controllers');
\Larakit\CRUD\Route::attach($r);
\Larakit\CRUD\Route::thumb($r);
\Larakit\CRUD\Route::thumbs($r);

//################################################################################
//  меню
//################################################################################
\Larakit\Widgets\WidgetSidebarMenu::group('Справочники')
    ->addItem('snippet', 'Название', ROUTE_ADMIN_SNIPPET);

//################################################################################
//  SEO
//################################################################################
\Larakit\SEO::title(ROUTE_ADMIN_SNIPPET, 'Название');