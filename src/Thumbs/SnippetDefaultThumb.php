<?php
namespace Larakit\Thumbs;

use Larakit\Thumb\Thumb;
use Larakit\Thumb\ThumbSize;

class SnippetDefaultThumb extends Thumb {

    static function getName() {
        return 'Название типа';
    }

    function getPrefix() {
        return 'vendor/entity';
    }

    function getSizesList() {
        return [
            'sm' => ThumbSize::factory('Маленький')
                ->setW(100)
                ->setH(100)
                ->filterAdd(Thumb::FILTER_CROP_BOX_IN_IMG),
            'lg' => ThumbSize::factory('Большой')
                ->setW(1200)
                ->setH(900)
                ->filterAdd(Thumb::FILTER_CROP_BOX_IN_IMG),
        ];
    }
}