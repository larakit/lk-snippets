<?php
namespace Larakit\QuickForms;

use Larakit\Models\Snippet;
use Larakit\QuickForm\ILaraForm;
use Larakit\QuickForm\LaraForm;
use Larakit\Snippets\ManagerSnippet;
use Larakit\Validators\SnippetValidator;

class SnippetForm extends LaraForm implements ILaraForm {

    /**
     * @return LaraForm
     */
    static function factory() {
        return new SnippetForm('snippet');
    }

    function build() {
        $this
            ->putTextTwbs('code')
            ->setLabel('Код сниппета')
            ->setDesc('Вставить сниппет можно будет в шаблон так: {{ larakit_snippet("code") }}');
        $el    = $this
            ->putTextareaTwbs('html')
            ->setRows(10)
            ->setLabel('Код');
        $id    = \Request::route('id');
        if($id){
            $model = Snippet::find($id);
            if($model){
                $el->setExample(ManagerSnippet::getDefault($model->code));
            }
        }
    }

    function getValidatorClass() {
        return SnippetValidator::class;
    }

}