<?php
namespace Larakit\Snippets;

use Illuminate\Support\Arr;

/**
 * Created by PhpStorm.
 * User: koksharov
 * Date: 01.12.16
 * Time: 14:26
 */
class ManagerSnippet {

    protected static $default = [];
    protected static $values  = [];

    /**
     * Зарегистрировать дефолтное значение
     * Оно будет выводиться - если не создание кастомное, чтобы в шаблонах не было пустоты
     * А также в форме редактирования как пример
     * @param $code
     * @param $html
     */
    static function registerDefault($code, $html) {
        static::$default[$code] = $html;
    }

    static function getDefault($code) {
        return Arr::get(static::$default, $code);
    }
}