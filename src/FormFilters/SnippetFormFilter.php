<?php
namespace Larakit\FormFilters;

use Larakit\FormFilters\FilterLike;
use Larakit\FormFilters\FormFilter;

class SnippetFormFilter extends FormFilter {

    function init() {
        $this->addFilter(
            FilterLike::factory('name')
                ->label('Название')
        );
    }
}