<?php
namespace Larakit\Validators;

use Larakit\ValidateBuilder;

class SnippetValidator extends ValidateBuilder {

    function build() {
        $this
            //############################################################
            //составляем правила для поля "agree"
            //############################################################
            ->to('html')
            ->ruleRequired('Обязательное поле')
            ->to('code')
            ->ruleRequired('Обязательное поле')
            ->ruleUnique('larakit__snippets', 'code', (int)\Request::route('id'), 'Не уникально!')
        ;
    }

}