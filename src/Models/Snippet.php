<?php
namespace Larakit\Models;

use Larakit\Attaches\TraitModelAttach;
use Larakit\CRUD\TraitEntity;
use Larakit\Thumb\TraitModelThumb;
use Larakit\Thumb\TraitModelThumbs;

/**
 * @mixin \Eloquent
 */
class Snippet extends LarakitModel {

    use TraitEntity;
    use TraitModelAttach;
    use TraitModelThumbs;
    use TraitModelThumb;

    protected $default_sort = [
        'code' => 'asc',
    ];

    protected $table    = 'larakit__snippets';
    protected $perPage  = 100;
    protected $fillable = [
        'code',
        'html',
    ];
    protected $hidden   = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    function __toString() {
        return $this->code;
    }

    /**
     * @return string
     */
    static function messageNotFound() {
        return 'Запись не найдена!';
    }

}

//Snippet::deleting(function ($model) {
//    if($model->id == 3) {
//        throw new ValidateException('3333333333');
//    }
//    if($model->id == 2) {
//        throw new ValidateException('22222222');
//    }
//    if($model->id == 1) {
//        throw new ValidateException('111111');
//    }
//});